The solution was written in JAVA 11. It is compatible with JAVA 10+ due to the use of orElseThrow() which before required arguments.

All the exercises for the dev-test are in:
```
https://github.com/lsnova/dev-test/blob/master/README.md
```

The project is divided into four packages where three of them contain solution for exercises 1, 2 and 3 and the fourth one is for common classes.

Each package with the exercise has got it's own README.md file where you can find information how to run it.

For exercies 3 I also tried to break down a little bit how the algorithm works.



