package exercises.exercise3;

import exercises.common.OutputPrinter;
import exercises.exercise3.model.Edge;
import exercises.exercise3.model.Graph;
import exercises.exercise3.model.InputModel;

import java.util.*;
import java.util.stream.IntStream;

public class Exercise3 {
    private final InputModel inputModel;

    public Exercise3(InputModel inputModel) {
        this.inputModel = inputModel;
    }

    public static void main(String args[]) {
        final InputModel input = getInput();
        Exercise3 exercise = new Exercise3(input);
        exercise.run();
    }

    public void run() {
        Graph graph = new Graph();
        graph.initializeGraph(inputModel);
        final int numberOfConnectedComponents = findNumberOfConnectedComponents(graph);
        OutputPrinter.print(numberOfConnectedComponents);
    }

    int findNumberOfConnectedComponents(Graph graph) {
        int numberOfConnectedComponents = 0;
        for(Integer vertex: graph.getVertexAndConnectedOtherVertices().keySet()){
            if(!graph.getVisited().get(vertex)){
                graph.findDFS(graph, vertex);
                numberOfConnectedComponents++;
            }
        }
        return numberOfConnectedComponents;
    }

    private static InputModel getInput() {
        Scanner sc = new Scanner(System.in);
        int numberOfEdges = sc.nextInt();
        List<Edge> edges = new ArrayList<>();

        IntStream.range(0, numberOfEdges).forEach(i -> {
            int origin = sc.nextInt();
            int destination = sc.nextInt();
            edges.add(new Edge(origin, destination));
        });

        return new InputModel(edges);
    }
}