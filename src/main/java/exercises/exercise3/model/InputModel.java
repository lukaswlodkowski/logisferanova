package exercises.exercise3.model;

import java.util.List;

public class InputModel {
    private final List<Edge> edges;

    public InputModel(List<Edge> edges) {
        this.edges = edges;
    }

    public List<Edge> getEdges() {
        return edges;
    }
}
