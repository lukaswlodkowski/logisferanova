package exercises.exercise3.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Stack;

public class Graph {
    private final Map<Integer,
            HashSet<Integer>> vertexAndConnectedOtherVertices = new HashMap<>();
    private final Map<Integer,
            Boolean> visited = new HashMap<>();

    void addEdge(int origin, int destination) {
        vertexAndConnectedOtherVertices.putIfAbsent(origin, new HashSet<>());
        vertexAndConnectedOtherVertices.putIfAbsent(destination, new HashSet<>());
        vertexAndConnectedOtherVertices.get(origin).add(destination);
        vertexAndConnectedOtherVertices.get(destination).add(origin);
        visited.put(origin, false);
        visited.put(destination, false);
    }

    public void initializeGraph(InputModel inputModel) {
        inputModel.getEdges().forEach(edge -> addEdge(edge.getOne(), edge.getTwo()));
    }

    public void findDFS(Graph graph, int vertex) {
        Stack<Integer> stack = new Stack<>();
        stack.push(vertex);
        while (!stack.empty()) {
            vertex = stack.pop();
            final Boolean isVisited = graph.getVisited().get(vertex);
            if (isVisited) {
                continue;
            }
            graph.getVisited().put(vertex, true);
            final HashSet<Integer> vertices = graph.getVertexAndConnectedOtherVertices().get(vertex);
            for (Integer v : vertices) {
                if (!graph.getVisited().get(v)) {
                    stack.push(v);
                }
            }
        }
    }

    public Map<Integer, Boolean> getVisited() {
        return visited;
    }

    public Map<Integer, HashSet<Integer>> getVertexAndConnectedOtherVertices() {
        return vertexAndConnectedOtherVertices;
    }
}
