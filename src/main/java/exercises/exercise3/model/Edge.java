package exercises.exercise3.model;

public class Edge {
    private final int one;
    private final int two;

    public Edge(int one, int two) {
        this.one = one;
        this.two = two;
    }

    public int getOne() {
        return one;
    }

    public int getTwo() {
        return two;
    }
}
