#How to run it?

To build:
```
mvn clean install
```
To run:
```
mvn exec:java -Dexec.mainClass=exercises.exercise3.Exercise3
```

Once the program is run you can start writing your input into the console.

Useful links which helped to understand the problem:
1) https://www.baeldung.com/cs/graph-connected-components
2) https://www.baeldung.com/cs/depth-first-traversal-methods
3) http://www.csl.mtu.edu/cs2321/www/newLectures/24_Graph_Terminology.html

#How the algorithm works?

Let's consider the following input
```
3
1 2
2 3
5 6
```

1. Getting all the necessary data
2. Initializing number of connected components (vertices of one graph) to 0
3. Creating a graph and adding all its edges. (Creating a map where key is vertice and value is a set of connected vertices)
    1. For above example the result is:
   ```
   key: 1 
   set values: 2
   ```
   ```
   key: 2 
   set values: 1, 3
   ```
   ```
   key: 3 
   set values: 2
   ```
   ```
   key: 5 
   set values: 6
   ```
   ```
   key: 6 
   set values: 5
   ```
    2. Note: while adding edges:
   ```
   verticeAndConnectedOtherVertices.get(origin).add(destination);
   verticeAndConnectedOtherVertices.get(destination).add(origin);
   ```
   we are adding origin to destination and destination to origin since it is undirected graph.
4. Checking for each vertice if it has been visited before.
    1. First vertice is 1: if it has not been visited then find DFS. Getting value from map which is 2. 
    Marking vertive 2 as visited and finding DFS for connected vertices to 2. It is 1 and 3 but 1 was visited already, hence we check for 3.
    Marking vertice 3 as visited and finding DFS for connected vertices to 3. It is 2 but was already visited. No more other vertices. Finishing first look and incrementing number of connected componentes to 1.
    2. Checking if vertice 2 was visited - yes. Continue.
    3. Checking if vertice 3 was visited - yes. Continue.
    4. Checking if vertice 5 was visited - No. Find DFS. Marking 5 as visited. Checking for other connected vertices to vertice 5. It is 6.
    Marking 6 as visited and finding DFS. No more connected vertices which has not been visited before. Getting out of the look and increment number of connected components to 2;
5. printing solution which in number of connected components.
    

