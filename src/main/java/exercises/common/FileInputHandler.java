package exercises.common;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileInputHandler implements InputHandler {
    private final String filePathAndName;
    private final String inputSplitter;

    public FileInputHandler(String inputSplitter, String filePathAndName) {
        this.inputSplitter = inputSplitter;
        this.filePathAndName = filePathAndName;
    }

    @Override
    public Integer[] getInput() {
        String content = "";
        try (Stream<String> lines = Files.lines(Paths.get(filePathAndName))) {
            content = lines.collect(Collectors.joining(System.lineSeparator()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Stream.of(content.split(inputSplitter))
                .mapToInt(Integer::parseInt)
                .sorted()
                .boxed()
                .toArray(Integer[]::new);
    }
}
