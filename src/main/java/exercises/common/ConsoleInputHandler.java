package exercises.common;

import java.util.Scanner;
import java.util.stream.Stream;

public class ConsoleInputHandler implements InputHandler {
    private final String inputSplitter;

    public ConsoleInputHandler(String inputSplitter) {
        this.inputSplitter = inputSplitter;
    }

    @Override
    public Integer[] getInput() {
        String input = receiveInput();
        return parseStringToInteger(input);
    }

    private String receiveInput() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    private Integer[] parseStringToInteger(String input) {
        String[] splittedIntegers = input.split(inputSplitter);
        return Stream.of(splittedIntegers)
                .mapToInt(Integer::parseInt)
                .sorted()
                .boxed()
                .toArray(Integer[]::new);
    }
}
