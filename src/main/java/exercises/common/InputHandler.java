package exercises.common;

public interface InputHandler {
    Integer[] getInput();
}
