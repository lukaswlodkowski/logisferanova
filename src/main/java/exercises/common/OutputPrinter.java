package exercises.common;

import java.util.Arrays;

public class OutputPrinter {
    public static final String SEPARATOR = " ";

    public static void print(int firstNumber, int secondNumber, String separator) {
        StringBuilder stringBuilder = new StringBuilder();
        StringBuilder pairOfNumbers = stringBuilder.append(firstNumber).append(separator).append(secondNumber);
        System.out.println(pairOfNumbers);
    }

    public static void print(Integer[] integers, String separator) {
        final StringBuilder stringBuilder = new StringBuilder();
        Arrays.stream(integers).forEach(integer -> stringBuilder.append(integer).append(separator));
        System.out.println(stringBuilder.toString());
    }

    public static void print(int numberOfConnectedComponents) {
        System.out.println(numberOfConnectedComponents);
    }
}
