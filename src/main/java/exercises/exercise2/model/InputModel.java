package exercises.exercise2.model;

public class InputModel {
    private final Integer[] integers;
    private final int sumOfTwoNumbers;

    public InputModel(Integer[] integers, int sumOfTwoNumbers) {
        this.integers = integers;
        this.sumOfTwoNumbers = sumOfTwoNumbers;
    }

    public int getSumOfTwoNumbers() {
        return sumOfTwoNumbers;
    }

    public Integer[] getIntegers() {
        return integers;
    }
}
