#How to run it?

To build:
```
mvn clean install
```
To run:
```
mvn exec:java -Dexec.mainClass=exercises.exercise2.Exercise2 -Dexec.args="' ' 'src\main\resources\inputExercise2.txt' 13"
```

We are passing here two arguments.
1. First is for defining splitting character for input.
2. Second is for specifying the file path and name. The extension of a file I used for this purpose is .txt. The file with example input can be found in the resources.
3. Third is for specifying sum of two numbers. For this task it was set to 13.