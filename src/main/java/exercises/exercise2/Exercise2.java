package exercises.exercise2;

import exercises.common.FileInputHandler;
import exercises.common.InputHandler;
import exercises.common.OutputPrinter;
import exercises.exercise2.model.InputModel;
import exercises.exercise2.model.Pair;

import java.util.*;

public class Exercise2 {
    private final InputModel inputModel;

    public Exercise2(InputModel inputModel) {
        this.inputModel = inputModel;
    }

    public static void main(String[] args) {
        final InputHandler inputHandler = new FileInputHandler(args[0], args[1]);
        final InputModel inputModel = new InputModel(inputHandler.getInput(), Integer.parseInt(args[2]));
        final Exercise2 exercise = new Exercise2(inputModel);
        exercise.run();
    }

    public void run() {
        final List<Pair> pairs = createListOfPairs();
        printPairs(pairs);

    }

    List<Pair> createListOfPairs() {
        final ArrayList<Pair> pairs = new ArrayList<>();
        Integer[] inputArray = inputModel.getIntegers();
        final Set<Integer> uniqueIntegersFromInputArray = new HashSet<>(Arrays.asList(inputArray));
        int difference;
        for (Integer integer : inputArray) {
            difference = inputModel.getSumOfTwoNumbers() - integer;
            if (uniqueIntegersFromInputArray.contains(difference)) {
                pairs.add(new Pair(integer, difference));
                uniqueIntegersFromInputArray.remove(integer);
            }
        }
        return pairs;
    }

    private void printPairs(List<Pair> pairs) {
        pairs.forEach(pair -> OutputPrinter.print(pair.getFirstNumber(), pair.getSecondNumber(), OutputPrinter.SEPARATOR));
    }
}
