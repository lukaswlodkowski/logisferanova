package exercises.exercise1;

import exercises.common.FileInputHandler;
import exercises.common.InputHandler;
import exercises.exercise1.model.BasicMeasurements;

public class Exercise1 {
    private final BasicMeasurements basicMeasurements;

    public Exercise1(BasicMeasurements basicMeasurements) {
        this.basicMeasurements = basicMeasurements;
    }

    public static void main(String[] args) {
        final InputHandler inputHandler = new FileInputHandler(args[0], args[1]);
        final Exercise1 exercise = new Exercise1(new BasicMeasurements(inputHandler.getInput()));
        exercise.run();
    }

    public void run() {
        basicMeasurements.printBasicMeasurements();
    }
}
