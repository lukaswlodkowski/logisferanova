#How to run it?

To build:
```
mvn clean install
```
To run:
```
mvn exec:java -Dexec.mainClass=exercises.exercise1.Exercise1 -Dexec.args="' ' 'src\main\resources\inputExercise1.txt'"
```

We are passing here two arguments.
1. First is for defining splitting character for input.
2. Second for specifying the file path and name. The extension of a file I used for this purpose is .txt. The file with example input can be found in the resources.