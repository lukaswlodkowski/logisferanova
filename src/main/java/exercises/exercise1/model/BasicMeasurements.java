package exercises.exercise1.model;

import exercises.common.OutputPrinter;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;

public class BasicMeasurements {
    private final Integer[] integers;
    private final int count;
    private final int distinct;
    private final int min;
    private final int max;

    public BasicMeasurements(Integer[] integers) {
        this.integers = integers;
        this.count = integers.length;
        this.distinct = new HashSet<>(Arrays.asList(integers)).size();
        this.min = Arrays.stream(integers).min(Comparator.naturalOrder()).orElseThrow();
        this.max = Arrays.stream(integers).max(Comparator.naturalOrder()).orElseThrow();
    }

    public void printBasicMeasurements() {
        OutputPrinter.print(integers, OutputPrinter.SEPARATOR);
        System.out.println("count: " + count);
        System.out.println("distinct: " + distinct);
        System.out.println("min: " + min);
        System.out.println("max: " + max);
    }

    public Integer[] getIntegers() {
        return integers;
    }

    public int getCount() {
        return count;
    }

    public int getDistinct() {
        return distinct;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }
}
