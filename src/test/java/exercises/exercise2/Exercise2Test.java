package exercises.exercise2;

import exercises.exercise2.model.InputModel;
import exercises.exercise2.model.Pair;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

class Exercise2Test {
    @Test
    void test() {
        final InputModel inputModel = new InputModel(new Integer[]{1, 2, 10, 7, 5, 3, 6, 6, 13, 0}, 13);
        final List<Pair> pairs = new ArrayList<>();
        pairs.add(new Pair(0, 13));
        pairs.add(new Pair(3, 10));
        //pairs.add(new Pair(6, 7));
        pairs.add(new Pair(6, 7));
        Exercise2 exercise = new Exercise2(inputModel);

        final List<Pair> listOfPairs = exercise.createListOfPairs();

        listOfPairs.forEach(pair -> assertTrue(pairs.contains(pair)));
    }
}