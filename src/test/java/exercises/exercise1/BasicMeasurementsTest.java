package exercises.exercise1;

import exercises.exercise1.model.BasicMeasurements;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


class BasicMeasurementsTest {
    @Test
    void shouldReturnAllBasicMeasurements() {
        final Integer[] integers = {1, 2, 4, 3, 3};
        BasicMeasurements basicMeasurements = new BasicMeasurements(integers);

        assertThat(basicMeasurements.getIntegers()).isEqualTo(integers);
        assertThat(basicMeasurements.getCount()).isEqualTo(5);
        assertThat(basicMeasurements.getDistinct()).isEqualTo(4);
        assertThat(basicMeasurements.getMin()).isEqualTo(1);
        assertThat(basicMeasurements.getMax()).isEqualTo(4);
    }
}