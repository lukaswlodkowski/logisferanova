package exercises.exercise3;

import exercises.exercise3.model.Edge;
import exercises.exercise3.model.Graph;
import exercises.exercise3.model.InputModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

class Exercise3Test {

    private final List<Edge> edges = new ArrayList<>();
    private InputModel inputModel;

    @BeforeEach
    public void setUp() {
        final Edge edge1 = new Edge(1, 5);
        final Edge edge2 = new Edge(2, 5);
        final Edge edge3 = new Edge(3, 6);
        final Edge edge4 = new Edge(4, 10);
        final Edge edge5 = new Edge(5, 8);
        final Edge edge6 = new Edge(6, 7);
        final Edge edge7 = new Edge(8, 9);
        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);
        edges.add(edge6);
        edges.add(edge7);
        inputModel = new InputModel(edges);
    }

    @Test
    void shouldReturnNumberOfConnectedComponents() {
        final Graph graph = new Graph();
        graph.initializeGraph(inputModel);

        Exercise3 exercise = new Exercise3(inputModel);
        final int numberOfConnectedComponents = exercise.findNumberOfConnectedComponents(graph);

        assertThat(numberOfConnectedComponents).isEqualTo(3);
    }

}