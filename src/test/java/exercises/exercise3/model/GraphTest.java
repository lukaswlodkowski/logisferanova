package exercises.exercise3.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

class GraphTest {
    private Map<Integer, HashSet<Integer>> vertexAndConnectedOtherVertices;

    @BeforeEach
    public void setUp(){
        final HashSet<Integer> connectedVertices1 = new HashSet<>();
        final HashSet<Integer> connectedVertices2 = new HashSet<>();
        final HashSet<Integer> connectedVertices3 = new HashSet<>();
        final HashSet<Integer> connectedVertices4 = new HashSet<>();
        connectedVertices1.add(5);
        connectedVertices2.add(1);
        connectedVertices3.add(3);
        connectedVertices4.add(4);

        vertexAndConnectedOtherVertices = new HashMap<>();
        vertexAndConnectedOtherVertices.put(1, connectedVertices1);
        vertexAndConnectedOtherVertices.put(5, connectedVertices2);
        vertexAndConnectedOtherVertices.put(4, connectedVertices3);
        vertexAndConnectedOtherVertices.put(3, connectedVertices4);
    }

    @Test
    void shouldReturnVerticesAndAllOtherConnectedToItVerticesAndSetThemAsNotVisited() {
        Map<Integer, Boolean> visited = new HashMap<>();
        visited.put(1, false);
        visited.put(5, false);
        visited.put(3, false);
        visited.put(4, false);

        Graph graph = new Graph();
        graph.initializeGraph(new InputModel( new ArrayList<>()));
        graph.addEdge(1, 5);
        graph.addEdge(4, 3);

        assertThat(graph.getVertexAndConnectedOtherVertices()).isEqualTo(vertexAndConnectedOtherVertices);
        assertThat(graph.getVisited()).isEqualTo(visited);
    }

    @Test
    void shouldMarkAllVerticesAsVisitedForVertex1(){
        Map<Integer, Boolean> visited = new HashMap<>();
        visited.put(1, true);
        visited.put(5, true);
        visited.put(3, false);
        visited.put(4, false);

        Graph graph = new Graph();
        graph.initializeGraph(new InputModel( new ArrayList<>()));
        graph.addEdge(1, 5);
        graph.addEdge(4, 3);

        graph.findDFS(graph,1);

        assertThat(graph.getVisited()).isEqualTo(visited);
    }
}